class IndicatorsController < ApplicationController
	
	def index
	end

	def show
		# function to get uf, dolar and tmc indicators from sbif API
		@pesos_uf = get_sbif_data("uf","UFs")
		@dolar =  get_sbif_data("dolar","Dolares")
		@tmc = get_sbif_data("tmc","TMCs")
		# get hash array to graph series
		@tmc_series = build_series_tmc(@tmc)
		# get hash array to graph uf indicators
		@pesos_graph = build_graph_data(@pesos_uf)
		# get hash array to graph dolar indicators
		@dolar_graph = build_graph_data(@dolar)
		# get hash array to graph tmc indicator
		# get max, min an prom values from uf and dolar indicators
		@uf_max = max_value(@pesos_graph)
		@dolar_max = max_value(@dolar_graph)
		@uf_min = min_value(@pesos_graph)
		@dolar_min = min_value(@dolar_graph)
		@uf_prom = prom_data(@pesos_graph)
		@dolar_prom = prom_data(@dolar_graph)
		#respond with js format to handle ajax response
		respond_to do |format|
	    	format.js   { }
	    end

	end

	def max_value(data)
		#receives hash array and return max value from array
		begin
			return (data.max_by{|k,v| v})[-1]
		rescue
		#return zero in case of error
			return 0
		end
	end

	def min_value(data)
		#receives hash array and return min value from array
		begin
			return (data.min_by{|k,v| v})[-1]
		rescue
		#return zero in case of error
			return 0
		end
	end

	def prom_data(data)
		#receives hash array and return prom data
		begin
			return (data.sum {|k,v| v }/data.size()).round(2)
		rescue
		#return zero in case of error
			return 0
		end
	end

	def name_serie(item)
		#receives an item of hash array and returns the name of TMC serie
		begin
			serie_name = (item["Titulo"] or "")+" "+(item["SubTitulo"] or "")+". Tipo "+(item["Tipo"] or "N/A")
			return serie_name
		rescue
		#return empty string in case of error
			return ""
		end
	end

	def build_series_tmc(data)
		#receives tmc data in array and build an array of hash arrays to graph
		result = []
		#loop in tmc data group by "Titulo","SubTitulo" and "Tipo"
		begin
			data.each do |item|
				# get first item to get the title of the serie
				first = item.first
				tmc_name = name_serie(first)
				#build the hash array of the serie to graph
				tmc_graph = build_graph_data(item)
				#push the item of the serie to graph with the name and the hash array
				result.push({name: tmc_name, graph: tmc_graph})
			end
			return result
		rescue
		#return empty array in case of error
			return result
		end
	end

	def peso_to_f(value)
		#receives a number with format ##.###,## and returns float of the value
		#example: receives 20.939,49 and return 20939.49
		begin
			value = value.gsub('.', '')
			value = value.gsub(',', '.')
			return value.to_f
		rescue
		#return zero in case of error
			return 0
		end
	end

	def build_graph_data(data)
		#receives a hash array and convert into hash 
		#array with the date of key and the indicator value to that date as its value
		begin
			graph_data = Hash[data.collect { |x| [x["Fecha"], peso_to_f(x["Valor"])] }]
			return graph_data
		rescue
		#return empty hash in case of error
			return {}
		end
	end

	def split_date(date)
		#receives a date in format dd/mm/yyyy and return date in array [dd,mm,yyyy]
		begin
			date = date.split("/")
			return date
		rescue
		#return zeros array in case of error
			return ["00","00","0000"]
		end
	end

	def parse_date_to_filter(day,month,year)
		#receives a day, month and a year in string and return a Date
		begin
			return Date.parse(year+"-"+month+"-"+day)
		rescue
		#returns nil in case of error
			return nil
		end
	end

	def filter_dates_in_response(data, start_date, end_date)
		#filter response from sbif API with dates given from the user
		begin
			return data.select {|x| (Date.parse(x["Fecha"]) >= start_date and Date.parse(x["Fecha"]) <= end_date) }
		rescue
			#return data without date filter in case of error
			return data
		end
	end

	def get_sbif_data(indicator,key)
		#receives the start and end date to get uf indicators from sbif API
		#split the start date to get day, month and year 
		date_ini = split_date(params[:dateini])
		day_ini = date_ini[0]
		month_ini = date_ini[1]
		year_ini = date_ini[2]
		#split the end date to get day, month and year
		date_fin = split_date(params[:datefin])
		day_fin = date_fin[0]
		month_fin = date_fin[1]
		year_fin = date_fin[2]
		#get the api sbif api key from enviroment var
		api_key = ENV["SBIF_API_KEY"]
		#get the base url of sbif api from enviroment var and build url to request with the start and end date
		url = ENV["SBIF_URL"]+indicator+"/periodo/"+year_ini+"/"+month_ini+"/"+year_fin+"/"+month_fin+"?apikey="+api_key+"&formato=json"
		begin
			#call to sbif API with url built
			response = RestClient.get url
			if response.code == 200
				#if response was successfully parse JSON
				response = JSON.parse(response)
				#convert date receives from user
				date_i = parse_date_to_filter(day_ini,month_ini,year_ini)
				date_f = parse_date_to_filter(day_fin,month_fin,year_fin)
				if date_i and date_f
					#if conver dates was successfully, filter hash array received from sbif with the dates entered by the user
					result = filter_dates_in_response(response[key], date_i, date_f)
					if indicator == "tmc"
						#if the indicator is tmc is necessary group by the "Titulo","SubTitulo" and "Tipo" to bulild series to graph
						begin 
							result = result.group_by {|x| [x["Titulo"],x["SubTitulo"],x["Tipo"]]}
							#get the arrays of each group of the serie
							result = result.values
						rescue
							#if fault the group of the data return error
							result = -1
						end
					end
					return result
				else
					return -1
				end
			else
				return -1
			end
		rescue
			return -1
		end
	end
end
