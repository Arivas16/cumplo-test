module IndicatorsHelper
	def pesos_to_graph
		line_chart @pesos_graph, library: {
			title: {text: 'Valor del UF por día', x: -20},
			yAxis: {
				title: {
					text: 'Valor (CLP)'
				},
				min: 10000,
        		max: 30000,
				scrollbar: {
					enabled: true
				}
			},
			xAxis: {
				title: {
					text: 'Fecha'
				},
				scrollbar: {
					enabled: true
				}
			}
		}
  	end

  	def dolar_to_graph
		line_chart @dolar_graph, library: {
			title: {text: 'Valor del dolar por día', x: -20},
			yAxis: {
				title: {
					text: 'Valor (CLP)'
				},
				min: 400,
        		max: 1000,
				scrollbar: {
					enabled: true
				}
			},
			xAxis: {
				title: {
					text: 'Fecha'
				},
				scrollbar: {
					enabled: true
				}
			}
		}
  	end

  	def tmc_to_graph
		bar_chart @tmc_series.map { |tmc|
		    {name: tmc[:name], data: tmc[:graph]}
		}, library: {
			title: {text: 'Valor del TMC por día', x: -20},
			yAxis: {
				title: {
					text: 'Valor (CLP)'
				}
			},
			xAxis: {
				title: {
					text: 'Fecha'
				},
				scrollbar: {
					enabled: true
				}
			}
		}
  	end
end
