# README

* Project Description

	This is a project to get finantial data from SBIF(Superintendencia de Bancos e Instituciones Financieras de Chile) API, which URL is defined in SBIF_URL enviroment var
	The finantial data to get are:
	-UF(Unidades de Fomento)
	-Dollar(in Chilean Peso)
	-TMC(Tasa Máxima de Interés Convencional)

	The project get from an user the start date and end date, this is the period to request the data to SBIF API.

	With the data received, the project build a graph and a table with the values per day in the period defined by the user. This process is done with UF and dollar data.

	With the TMC data, the graph is build with series, because the TMC data is group by its title, subtitle and tipo. The graph shows the differents values of each TMC serie in the period defined by the user.


* Ruby version
	- ruby-2.3.3

* Rails version
	- 5.1.1

* Configuration

	- Install Ruby Version Manager(RVM) with Ruby
	    Before any other step install mpapis public key (might need gpg2) (see security)
	    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
	    \curl -sSL https://get.rvm.io | bash -s stable --ruby
	    source /home/user/.rvm/scripts/rvm
	    rvm list to verify the correct installation (It must show list of installed ruby versions)
	    run rvm use ruby-2.3.3 or the rvm use ruby of installed version

    - Clone the project from repo
        Run git clone https://Arivas16@bitbucket.org/Arivas16/cumplo-test.git

    - Create the enviroments vars to SBIF API in your ~/.bashrc
		- open your bash file with sudo nano ~/.bashrc
		- copy the next two lines at the end of you ~/.bashrc file:
			- export SBIF_URL=http://api.sbif.cl/api-sbifv3/recursos_api/
			- export SBIF_API_KEY=d6a868c6e9f1de2250dcf7f20a84e1ff0b19cbb7
		- save your file
		- load the new variables run source ~/.bashrc


    - Install dependencies
        Run bundle install

    - Deploy project
    	Run rails s

    - Input
        Open in browser http://localhost:3000/
        Fill and send the form